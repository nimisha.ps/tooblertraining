const Sequelize = require("sequelize");

const con = require("../config/db2");


let userModel = con.define(
  "signup",
  {
    id: {
      type: Sequelize.INTEGER(30),
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },

    last_name: {
      type: Sequelize.STRING(45),
      allowNull: false,
    },
    email_id: {
      type: Sequelize.STRING(40),
      allowNull: false,
    },
    phone_no: {
      type: Sequelize.STRING(10),
      allowNull: false,
    },
    gender: {
      type: Sequelize.STRING(30),
      allowNull: false,
    },
    state: {
      type: Sequelize.STRING(40),
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING(200),
      allowNull: false,
    },
    secret: {
      type: Sequelize.STRING(200),
      allowNull: false,
    }
  },
  {
    freezeTableName: true,
    timestamps: false,
  }
);

module.exports.userModel = userModel;
