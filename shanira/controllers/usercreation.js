
var encrypt = require('../helpers/encryption');
const userModel = require("../models/usermodel");

const signup = async function (req, res) {
    try {
        let array = ["fname", "lname", "email", "phone", "gender", "state", "pwd", "subscribe"]

        array.forEach((field) => {
            console.log(field)
            if ((!req.body[field]) || (req.body[field].trim() == "")) {
                throw (`error:${field} is required`)
            }
        })
        var regex = /^[A-Za-z]+$/;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var phregx = /^[0-9-+]+$/;
        var psregx = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/
        if (regex.test(req.body.fname) == false) {
            throw ("error:name must contain only alphabets")
        }
        else if (emailReg.test(req.body.email) == false) {
            throw ("error:Email pattern not matching")
        }
        else if (regex.test(req.body.lname) == false) {
            throw ("error:Last name must contain only alphabets ")
        }

        else if (phregx.test(req.body.phone) == false) {
            throw ("error:phone number must match pattern")
        }
        else if (psregx.test(req.body.pwd) == false) {
            throw ("error:password must contain Minimum eight characters, at least one letter, one number and one special character:")
        }
        else if (req.body.pwd != req.body.cpp) {
            throw ("error:Your password and confirmation password do not match.");
        }
        var sec = Math.random().toString(36).slice(-8);
        let hashedPassword = encrypt(req.body.pwd, sec)
        return await userModel.create({
            fname: req.body.fname,
            lname: req.body.lname,
            email: req.body.email,
            phno: req.body.phone,
            gender: req.body.gender,
            state: req.body.state,
            password: hashedPassword,
            subscribe: Boolean(req.body.subscribe),
            secret: sec
        }).then(function (userModel) {
            if (userModel) {
                res.send(userModel);
            } else {
                res.status(400).send('Error in insert new record');
            }
        });



    }
    catch (e) {
        res.send(e)
    }
}
module.exports = signup