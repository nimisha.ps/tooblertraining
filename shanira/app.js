var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var user=require('./routes/user')

const cors = require("cors")
app.use(cors())


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
app.use("/user",user)
app.listen(8080, function () {
    console.log('Example app listening at 8080');
    console.log("Connected")
  });  