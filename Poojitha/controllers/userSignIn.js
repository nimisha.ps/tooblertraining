const { userModel } = require("../models/userModel");
const crypto = require('crypto');
const {generateAccessToken}=require('../controllers/Helper')


const userSigIn = async function (req, res) {
    try {
        const username = req.body.email
        const pass = req.body.pass
        // console.log(username,pass)
        const data = await userModel.findOne({
            where: {
                email: username
            }
        });
        const secret = data.secret
        const password = data.pass
        // console.log(pass,password,secret)
        const hash = crypto.createHmac('sha256', secret)
            .update(pass)
            .digest('hex');
            console.log(hash,password)
        if (hash == password) {
            //    jwt token generation
            
            const token = generateAccessToken({ username: username });
            res.status(200).json({
                token:token
            });
           


                  }
        else res.status(400).json({
            status: "error",
            message: "username or password wrong"
        })
    }
    catch (e) {
        res.status(400).json({
            status: "error",
            message: e
        })
    }
}



module.exports = userSigIn