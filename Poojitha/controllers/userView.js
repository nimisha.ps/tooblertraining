const { userModel } = require("../models/userModel");


const userView = async function (req, res) {
    try {

        const users = await userModel.findAll();
        res.status(200).json(users)
    }
    catch (e) {
        res.send(e)
    }
}

const userViewById = async (req, res) => {
    try {
       
       const data= await userModel.findOne({
            where: {
              id: req.params.id
            }
          });
          res.status(200).json(data)
        
    }
    catch (e) {
        res.send(e)
    }

}
module.exports = { userView, userViewById }
