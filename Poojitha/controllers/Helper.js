const jwt = require('jsonwebtoken');
const TOKEN_SECRET = 'a5e87caeddd951eea1d197ebf6c4d9d1eb4bc1c47f49f9acb74b82b231b455a1a6d45a7c2e2fd758215f25952b3c18fc9854fb330a1328a40d215c295d1af640'

const generateAccessToken = (username) => {
    console.log(TOKEN_SECRET)
    console.log('hai')

    return jwt.sign(username, TOKEN_SECRET, { expiresIn: '5000s' });

}

// jwt token autherization

const authenticateToken = (req, res, next) => {
    try{
    console.log(req.headers.authorization)
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    // console.log(token)

    if (token == null) throw({
        status:401,
        message:"Unautherized User"
    })

    jwt.verify(token, TOKEN_SECRET, (err, token) => {
        console.log(err)

        if (err) 
       throw({
            status:403,
            message:"Authentication Failed"
        })

      

        next()
    })
}
catch(e){
    res.status(e.status).json({
        status:"error",
        message:e.message
    })
}
}





module.exports = { generateAccessToken, authenticateToken }