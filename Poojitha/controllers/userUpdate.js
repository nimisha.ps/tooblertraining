const { userModel } = require("../models/userModel");


const userUpdate = async function (req, res) {
    try {
        console.log("req.body")
        var newItem={
            f_name:req.body.f_name,
            l_name:req.body.l_name
            // email:req.body.email,
            // phone:req.body.phone,
            // gender:req.body.gender,
            // state:req.body.state,
            // pass:req.body.pass,
            // sub:req.body.sub
        }

       await userModel.update( newItem,{ where: {id: req.params.id}}).then(data=>{
            if(data==1)res.status(200).json({
                status:"success",
                message:"row updated"
            })
            throw({
                status:"error",
                message:"id not found"
            })
       });
        // return {data, created: false};
        // 
    }
    catch (e) {
        res.status(400).json({
            status:"error",
            message:e
        })
    }
}

const userDelete = async function (req, res) {
    try {
        userModel.destroy({
            where: {
                id: req.params.id
            }
        })
        .then( (deletedRecord)=> {
            if(deletedRecord === 1){
                res.status(200).json({
                    status:"success",
                    message:"Deleted successfully"});          
            }
            else
            {
                throw({
                    status:"error",
                    message:"record not found"})
            }
        })
    }
    catch (e) {
        res.status(400).json({
            status:"error",
            message:e
        })
    }
}

module.exports ={userUpdate,userDelete}