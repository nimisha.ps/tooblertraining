var express= require('express');

const {authenticateToken}=require('../controllers/Helper')
var userSignup = require("../controllers/user")
var {userView,userViewById}=require("../controllers/userView")
var {userUpdate,userDelete}=require('../controllers/userUpdate')
var userSignIn=require('../controllers/userSignIn')
var router= express.Router()

router.post('/signup',userSignup);
router.get('/view',authenticateToken,userView);
router.get('/view/:id',authenticateToken,userViewById)
router.put('/update/:id',authenticateToken,userUpdate)
router.delete('/delete/:id',authenticateToken,userDelete)
router.post('/signin',userSignIn)


module.exports=router