var bodyParser = require('body-parser');
var express = require('express');
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
var User1 = require('../models/usermodel');
var Role = require('../models/rolemodel');
const sequelize = require('../config/database');

var Sequelize = require('sequelize');


module.exports = {
   
    //insert role details
    insertRoles: async function (req, res, next) {

        const body = req.body;
        console.log(body);
        const role_name = body.role_name;
        
        if (!req.body.role_name|| req.body.role_name.trim() == "") {
            return res.status(400).json({ "error": "role name  is required" });
        } 
        else {
            

            return Role.create({
                role_name: role_name,
               
               

            }).then(function (Role) {
                if (Role) {
                    res.send(Role);
                    res.end()
                } else {
                    res.status(400).send('Error in insert new record');
                    res.end()
                }

            });



        }




    },
}
