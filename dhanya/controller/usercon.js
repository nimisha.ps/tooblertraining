var bcrypt = require('bcrypt');
var bodyParser = require('body-parser');
var express = require('express');
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
var User1 = require('../models/usermodel');
var Role = require('../models/rolemodel');
const sequelize = require('../config/database');
const jwt = require('jsonwebtoken');
var Sequelize = require('sequelize');

const { Op } = Sequelize;

module.exports = {

    //insert user details
    insertUserDetails: async function (req, res, next) {

        const body = req.body;
        console.log(body);
        const fname = body.fname;
        const lname = body.lname;
        const email = body.email;
        const phone = body.phone;
        const password = body.pwd;

        const gender = body.gender;
        const subscribe = Boolean(body.subscribe);
        const created_by = body.created_by;
        const status = body.status;
        const role_id = body.role_id;



        var salt;
        const hashPasswordAsync = async password => {
            salt = await bcrypt.genSalt();
            const hash = await bcrypt.hash(password, salt);
            console.log(hash)
            return hash;
        }

        const pwd = await hashPasswordAsync(password);


        if (!req.body.fname || req.body.fname.trim() == "") {
            return res.status(400).json({ "error": "first name  is required" });
        } else if (!req.body.lname || req.body.lname.trim() == "") {
            return res.status(400).json({ "error": "last name  is required" });
        } else if (!req.body.email || req.body.email.trim() == "") {
            return res.status(400).json({ "error": "Email is required" });
        } else if (!req.body.phone || req.body.phone.trim() == "") {
            return res.status(400).json({ "error": "Phone number is required" });
        }
        else if (!req.body.pwd || req.body.pwd.trim() == "") {
            return res.status(400).json({ "error": "Password is required" });
        } else if (!req.body.gender || req.body.gender.trim() == "") {
            return res.status(400).json({ "error": "Select gender" });
        } else if (!req.body.created_by || req.body.created_by.trim() == "") {
            return res.status(400).json({ "error": "Password is required" });
        }
        else if (!req.body.status || req.body.status.trim() == "") {
            return res.status(400).json({ "error": "Password is required" });
        }
        else {


            return User1.create({
                first_name: fname,
                last_name: lname,
                email: email,
                phone_number: phone,
                password: pwd,
                password_salt: salt,
                gender: gender,
                subscribe: subscribe,
                created_by: created_by,
                status: status,
                role_id: role_id


            }).then(function (User1) {
                if (User1) {
                    res.send(User1);
                    res.end()
                } else {
                    res.status(400).send('Error in insert new record');
                    res.end()
                }

            });



        }




    },




    //search and join together
    getUsers: (req, res) => {

        let condition = {};
        

        if (req.query.search) {
            let searchQuery = req.query.search;
            console.log(searchQuery)


            condition = {
                [Op.or]: [
                    { first_name: { [Op.like]: `%${searchQuery}%` } },
                    { last_name: { [Op.like]: `%${searchQuery}%` } },
                    { email: { [Op.like]: `%${searchQuery}%` } }
                ]
            };
        }
        if(req.query.status){
            condition["status"]=req.query.status;

        }
        if(req.query.gender){
            condition["gender"]=req.query.gender;

        }
        if(req.query.role_id){
            condition["role_id"]=req.query.role_id;

        }


        var order=['createdAt','DESC'];
        if(req.query.sort_field && req.query.sort_order)
        {
            let sort_field=req.query.sort_field;
        let sort_order=req.query.sort_order;
         order=[sort_field,sort_order];

        }
    
        
        
       
        


        User1.findAll({
            where:condition,
            attributes: ["id","first_name", "last_name", "email", "gender", "status"],
            offset:parseInt(req.query.offset),
            limit:parseInt(req.query.limit),
            order:[order],
           
            include: {
                model: Role,
                attributes: ['role_name'],
                required: true,
                
            }
        })
            .then((user) => {
                return res.status(200).json({ user })
            }).catch(err => {
                return res.status(400).json({ err })
            })


        


    },
















    
    //update user
    updateUser: (req, res) => {

        var id = req.params.id;

        User1.findOne({
            where: { id: id }
        }).then(user => {
            if (user) {
                user.update(req.body, { where: { id: id } })
                    .then((updateUser) => {
                        return res.status(202).json({
                            "message": "User updated successfully",
                            updateUser
                        })
                    })
            } else {
                return res.status(206).json({
                    "message": "User not found"
                })
            }
        }).catch(error => {
            return res.status(400).json({
                "error": error
            })
        })
    },
    

    //delete users
    deleteUsers: (req, res) => {

        let urlid = [];
        urlid = req.params['id'];
        console.log(urlid);
        let urlidd = [];
        urlidd = urlid.split(' ');
        console.log(urlidd);

        User.destroy({
            where: {

                id: urlidd
            }
        }).then(() => {
            return res.status(200).json({
                "message": "Users Deleted successfully"
            })
        })

    },
    // //Authenticate login



    login: async function (req, res) {
        if (!req.body.email || req.body.email.trim() == "") {
            return res.status(400).json({ "error": "email  is required" });
        } else if (!req.body.pwd || req.body.pwd.trim() == "") {

            return res.status(400).json({ "error": "password  is required" });
        }
        const logemail = req.body.email;
        const logpassword = req.body.pwd;

        const user = await User1.findOne({
            attributes: ['id', 'email', 'password', 'password_salt'],

            where: { email: logemail }
        })
        if (user) {
            const dbemail = user.email;
            //console.log(dbemail);
            const dbpw = user.password;
            console.log(dbpw);


            const dbsalt = user.password_salt;
            // console.log(dbsalt);
            const hashedpwd = await bcrypt.hash(logpassword, dbsalt);
            console.log(hashedpwd);
            if (hashedpwd == dbpw) {
                //res.send("Successfully logged in");
                let result = {
                    id: user.id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    email: user.email
                }


                let token = jwt.sign(result, "secretkey", { expiresIn: 86400 });
                res.status(200).send({ authentication: true, token: token });


            }
            else {
                res.send("Error in login");
            }
        } else {
            res.send("This user doesnot exists");
        }

    },
    // token verification

    verifyToken: (req, res, next) => {
        const authHeader = req.headers.authorization;
        if (authHeader == undefined) {
            res.status(401).send({ error: "No token provided" });
        }
        let token = authHeader.split(" ")[1];
        jwt.verify(token, "secretkey", function (err, decoded) {
            if (err) {
                res.status(401).send({ error: "Authentication failed" });
            }
            else {
                req = decoded;
                //res.send(decoded);
            }


        }); next();


    },




}





