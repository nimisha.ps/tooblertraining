



let sequelize = require('../config/database')
let Sequelize = require('sequelize');
const Role = require('./rolemodel');

const User1 = sequelize.define('User1', {
    id: {
        primaryKey: true,
        autoIncrement:true,
        type: Sequelize.INTEGER,
    },
  first_name:{
    allowNull:false,
    type:Sequelize.STRING,
  },
  last_name:{
    allowNull:false,
    type:Sequelize.STRING,

  },
  email:
  {
    allowNull:false,
    unique: true,
    type:Sequelize.STRING,

  },
  phone_number: {
    allowNull:false,
    unique: true,
    type:Sequelize.STRING,
  },
  password:{
    allowNull:false,
    type:Sequelize.STRING,
  },
  password_salt:{
    allowNull:false,
    type:Sequelize.STRING,
  },
  gender:{
    type: Sequelize.ENUM("male", "female", "others"),
    allowNull:false,
  },
  subscribe:{
    type:Sequelize.BOOLEAN,
    defaultValue:false
  },
 created_by:{
  type: Sequelize.INTEGER,
  allowNull:false,
  // references: {
  //   table: User1,
  //   field: id
  // }
 },
 status:{
  type: Sequelize.ENUM("active", "inactive", "trash"),
  defaultValue:"active",
  allowNull:false,

 },
 role_id:{
  type: Sequelize.INTEGER,
  defaultValue:1,
  allowNull:false,
  references: {
    model:'Role',
    key:'id'
    
  }

 },
//  created_at:Sequelize.DATE,
//   updated_at:Sequelize.DATE
  
}, {freezeTableName: true});
  User1.sync()

  Role.hasMany(User1, { foreignKey: 'role_id' });
  User1.belongsTo(Role, { foreignKey: 'role_id' });
module.exports = User1;