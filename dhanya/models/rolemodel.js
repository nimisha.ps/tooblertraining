



var sequelize = require('../config/database')
var Sequelize = require('sequelize')

const Role =sequelize.define('Role', {
    id: {
        primaryKey: true,
        autoIncrement:true,
        type: Sequelize.INTEGER,
    },
  role_name: {
    allowNull:false,
      type:Sequelize.STRING,
  },
//   created_at:Sequelize.DATE,
//   updated_at:Sequelize.DATE,
//   createdAt: false, 
//   updatedAt: false,
  
  
}, {freezeTableName: true});
  Role.sync();


module.exports = Role;