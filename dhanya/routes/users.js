
var express = require('express');
var app=express();

  const {
    
      insertUserDetails,
       updateUser,
       deleteUsers,
      login,  
    verifyToken,
    getUsers,
  } =require('../controller/usercon');


var router = express.Router();

// router.get('/form', function(req, res, next) { 
// res.render('users'); 
// });
 router.get('/loginform', function(req, res, next) { 
   res.render('login'); 
   });
router.post('/create',insertUserDetails);
 router.put('/updateuser/:id',verifyToken,updateUser);
 router.delete('/deleteusers/:id',verifyToken,deleteUsers);
router.post('/login',login);
router.get('/getusers',verifyToken,getUsers);
module.exports = router;
