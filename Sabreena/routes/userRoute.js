import express from 'express'
var router  = express.Router();

import userController from '../controllers/userController.js'

router.post('/signUp',userController.signUp)
router.post('/login',userController.Login)
router.post('/getUser',userController.getUser)

export default router;