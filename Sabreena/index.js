import express from 'express'
var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false })); 

import Role from './routes/roleRoute.js'
import User from './routes/userRoute.js'


app.use('/role',Role)
app.use('/user',User)
//app.use(cookieParser());

app.listen(3000,()=>{
    console.log('listening to port 3000');
})