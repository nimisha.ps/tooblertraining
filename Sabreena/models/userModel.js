import Sequelize from 'sequelize'
import sequelize from './server.js'
import Role from '../models/roleModel.js'

let User = sequelize.define('user-table',{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement : true
    },
    first_name:{
        type:Sequelize.STRING,
        allowNull:false
    },
    last_name:{
        type:Sequelize.STRING,
        allowNull:false

    },
    email:{
        type:Sequelize.STRING,
        allowNull:false,
        unique:true

    },
    phone_number:{
        type:Sequelize.INTEGER,
        allowNull:false,
        unique:true

    },
    password:{
        type:Sequelize.STRING,
        allowNull:false

    },
    password_salt:{
        type:Sequelize.STRING,
        allowNull:false

    },
    gender:{
        type:Sequelize.STRING,
        allowNull:false,
        isIn:['male','female','others']

    },
    subscribe:{
        type:Sequelize.BOOLEAN,
        defaultValue:false

    },
    // created_by:{
    //     type:Sequelize.INTEGER,
    //     allowNull:false

    // },
    status:{
        type:Sequelize.STRING,
        defaultValue:'active',
        isIn:['active','inactive','trash']

    }
},
{
    freezeTableName:true
})
User.belongsTo(Role,{
    foreignKey : 'role_id',
    allowNull:false
})
Role.hasOne(User,{foreignKey:'role_id'})

export default User