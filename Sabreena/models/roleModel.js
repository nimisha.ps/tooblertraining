import Sequelize, { Model } from 'sequelize'
import sequelize from './server.js'

let Role = sequelize.define('role-table',{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement : true
    },
    role:{
        type : Sequelize.STRING,
        // validate:{
        //     isIn:[['admin','super admin','customer']]
        // },
        allowNull:false
    }
},{
    freezeTableName:true
}
)

export default Role