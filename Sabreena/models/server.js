import dbConfig from "../config/dbConfig.js";
import Sequelize from 'sequelize'

const sequelize = new Sequelize(dbConfig.DB,dbConfig.USER,dbConfig.PASSWORD,{
    host : dbConfig.HOST,
    dialect : dbConfig.dialect,
    define : {timeStamp : false}

})

sequelize.authenticate().then(()=>{
    console.log('connected to db');
}).catch(err=>{
    console.log('Unable to connect to db');
})

const db = {}

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// sequelize.sync({force:false}).then(()=>{
//     console.log('re-sync done');
// })

export default sequelize