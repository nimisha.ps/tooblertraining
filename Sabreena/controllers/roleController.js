import dbRole from '../models/roleModel.js'

const addRole = async (req,res)=>{
try{
    const {body} = req
    const role = body.role

    if (!role || role === null)
        throw {message:"Missing Role"};

    if(role == 'admin' || role == 'super admin' || role == 'customer')
    { 
        let op = await dbRole.findOne({where:body})
        if(op){
            throw new Error('Role Already Exist..');
        }
        else{
            await dbRole.create(body)
            return res.status(200).json({
                message :"Role Added Successfully..."
            })
        } 

    }else throw {message:"Invalid Role"};    
}catch(err){
    return res.status(400).json({ error: err.message });

}


    
}

export default {addRole};
