import User from '../models/userModel.js'
import Role from '../models/roleModel.js'
import bcrypt from 'bcryptjs'
import jsonwebtoken from 'jsonwebtoken';
import dbConfig from '../config/dbConfig.js';
import Sequelize  from "sequelize";
const Op = Sequelize.Op;

const signUp = async (req,res)=>{
    try{
        const {body} = req
        const reqData = ['first_name','last_name','email','phone_number','password','gender']

        reqData.forEach(element => {
            if(!body[element] ||body[element] === null) 
            throw {message:'Field Missing '+ element };
        });
        if(!body['role_id'] || body['role_id'] == null) 
            {
            const roleId= await Role.findOne({where: {role:'customer'}})
            body.role_id = roleId.id
            }
                
        const salt =  bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(req.body.password,salt)
        body.password_salt = hash 
        
        const user = await User.create(body)
        return res.status(200).json({
            message :"SignUp Successfull..."
        })

    }catch(err){
        return res.status(400).json({ error: err.message });
    }
}

const Login = async(req,res)=>{
    try{
        const {body} = req
        const reqData = ["email","password"]
        reqData.forEach(field=>{
            if(!body[field] || body[field] === null) throw new Error('Field Missing '+ field);
        })
        const user =await User.findOne({where:{email:req.body.email}})
        console.log(user);
        if (!user || user == null) throw new Error('Email Id doesnt exist...')
        else{
            const compPassword = bcrypt.compareSync(req.body.password,user.password_salt)
            if(compPassword && user != null){
                var token =   jsonwebtoken.sign({
                    email:user.email,
                    id:user.id
                },
                dbConfig.JWT_TOKEN,(err,token)=>{
                    return res.status(200).json({
                        message :"Authentication Successful...",
                        token:token
                    })
                })
            }
            else throw new Error('Authentication failed')
         }
            
    }catch(e){
    return res.status(400).json({error :e.message})
   }
}

const getUser = async(req,res)=>{
try{
    let op;
    let condition = [
        {first_name: {[Op.like]: `%${req.query.search}%`} },
        {last_name: {[Op.like]: `%${req.query.search}%`} },
        {email: {[Op.like]: `%${req.query.search}%`}},
   ]
   let order_by = [['createdAt', 'ASC']]
   let attributes = ['first_name','last_name','email','status','gender','id','createdAt']
   
    if(req.query.status && req.query.gender) 
    {
        op = await User.findAll({
            attributes:attributes,
            where:{
                [Op.or]:condition,status:req.query.status,gender:req.query.gender
            },
            order:order_by 
        })

    }else if((req.query.status == undefined || req.query.status == null) || (req.query.gender == undefined || req.query.gender == null)) {
        op = await User.findAll({attributes:attributes,where:{[Op.or]:condition},order: order_by })

    }
            
    return res.status(200).json({
        message :op
    })
    }catch(e){
        return res.status(400).json({error :e.message})
    }

}
export default {signUp,Login,getUser}

