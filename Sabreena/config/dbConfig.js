export default{
 HOST : 'localhost',
 USER:'root',
 PASSWORD:'password',
 DB:'node-seq-project',
 dialect:'mysql',
 PORT:3000,
 JWT_TOKEN:'secret'
}