const express=require('express');
const app=express();

const router=require('./routes/user')
const bodyParser=require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/api",router)
app.listen(4000);