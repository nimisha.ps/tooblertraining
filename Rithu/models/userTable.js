const Sequelize = require('sequelize');
const sequelize = require('../config/sequilize');
let userModel = sequelize.define('log2', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    firstname: {
        type: Sequelize.STRING,
        allowNull: false,
    },

    lastname: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    mail: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    phonenum: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    gender: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    state: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    secret: {
        type: Sequelize.STRING,
        allowNull: false,
    }

},
    {
        freezeTableName: true, timestamps: false

    })
    module.exports=userModel;
