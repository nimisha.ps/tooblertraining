const { Router } = require('express');
const express=require('express');
const router=express.Router();
const user=require('../controllers/users')
const {jwtTokenVerify}=require('../helper/jwt')

router.get("/user/getpage",user.getpage)
router.get("/user/getLOGIN",user.getLoginPage)
router.post("/user/createUser",user.createUser)
router.post("/user/Login",user.createLogin)
router.get("/user/getusers",user.getAllUsers)
router.get("/user/getUser/:id",user.getUserById)
router.put("/user/updateUser/:id",jwtTokenVerify,user.updateUser)
router.delete("/user/deleteUser/:id",user.deleteUser)
module.exports=router;