var { userModel } = require('../Models/userModels');
const crypto = require('crypto');
const {jwtTokenGeneration}=require("../helper/jwtToken")
const userLogin = async (req, res) => {
    var username = req.body.Username
    var password = req.body.Password

    let user = await userModel.findOne({

        where:
            { Username: username }
    })
    const hash = crypto.createHmac('sha256', user.secret)
        .update(password)
        .digest('hex');
    console.log(hash,user.Password)
    if (hash == user.Password) {
        let token= jwtTokenGeneration(username)
        res.status(200).json({message:"log in successfully",tokenData : token})
    }
    else{
        res.send("Incorrect password")
    }

}
module.exports = userLogin


