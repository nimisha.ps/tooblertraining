const { json } = require("body-parser");
const {userModel} = require("../Models/userModels");

// Get all user deatails
const getUsers = async function (req, res) {
    try {
        await userModel.findAll().then(data => {
            console.log(data)
            res.status(200).json(data)
        })



    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
//get one user detail
const getuserByID = async function (req, res) {
    try {
        const id = req.params.id;
        await userModel.findOne({ where: { id: id } }).then(data => {
            if (data) { res.status(200).json(data) }
            else { throw ("requested user not present in the table") }
        })
    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
//delete a particular user details
const userDeletionByID = async function (req, res) {
    try {
        const id = req.params.id;
        await userModel.destroy({ where: { id: id } }).then(data => {
            if (data == 1) {
                res.status(200).send("user with id  " + id + " deletd sucessfully");

            }
            else { throw ("requested user not present in the table") }
        })
    }

     catch (e) {


        res.status(400).json({
             status: "error",
             message: e
         })
     }
 }
//update a user
const userUpdationByID = async function (req, res) {
    try {
        const id = req.params.id;
        const newitem = {
            Username:req.body.Username,
            Password:req.body.Password,
            Email: req.body.Email,
            Firstname:req.body.Firstname,
            Lastname:req.body.Lastname
            
        }
        await userModel.update(newitem, { where: { id: id } }).then(data => {
            if (data == 1) {
                res.status(200).send("user with id " + id + "updated sucessfully");

            }
            else { throw ("error:updating user with id :" + id) }
        })
    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
 module.exports = { getUsers, getuserByID, userDeletionByID, userUpdationByID }


 
// module.exports.getUsers=(req,res)=>{
//     userModel.findAll().then(data => {
//         res.status(200).json(data)
//     });
// }
// module.exports.getuserById=()=>{
//     const id = req.params.id;
//     userModel.findOne({where: {id:id}}).then(data => {
//         if (data) { res.status(200).json(data) }
//         else { throw ("requested user not present in the table") }
//     })
// }