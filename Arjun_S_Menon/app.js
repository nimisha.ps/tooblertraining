var express = require("express");
var app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var crd = require("./crude");

app.use("/login",crd.login);
app.use("/add",crd.enter);
app.listen(5002);